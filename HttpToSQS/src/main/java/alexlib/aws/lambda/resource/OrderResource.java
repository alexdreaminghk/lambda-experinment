package alexlib.aws.lambda.resource;


import java.time.Clock;
import java.util.Date;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import alexlib.aws.lambda.model.Order;
import alexlib.aws.lambda.model.SuccessfulResponse;

@Path("/order")
public class OrderResource {

    Logger logger = LoggerFactory.getLogger(OrderResource.class);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response ping(Order order) {

        AmazonSQS sqs = AmazonSQSClientBuilder.standard()
                            .withRegion(System.getenv("AWS_REGION"))
                            .build();

        
        Clock clock = Clock.systemUTC();
        Date now = Date.from(clock.instant());

        //set orderTime and then
        order.setOrderDate(now);
        //save order to SQS and return 200

        String payload;
        try {
            payload = new ObjectMapper().writeValueAsString(order);
        } catch (JsonProcessingException e) {
            logger.error("Serialization failure of Order Object",e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        // Map<String, MessageAttributeValue> messageAttributes = new HashMap<>();
        // messageAttributes.put("AttributeOne", new MessageAttributeValue()
        //   .withStringValue("This is an attribute")
        //   .withDataType("String")); 

        SendMessageRequest sqsRequest = new SendMessageRequest().withQueueUrl("https://sqs.eu-west-2.amazonaws.com/518750638386/orderQueue")
        .withMessageBody(payload)
        .withDelaySeconds(30);
      
        sqs.sendMessage(sqsRequest);

        SuccessfulResponse response = new SuccessfulResponse();
        response.setResult("Order placed successfully.");

        return Response.status(200).entity(response).build();
    }
}
