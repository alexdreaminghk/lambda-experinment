# Lambda-Experinment

API Gateway-->Lambda-->SQS-->Lambda-->DynamoDB

A restful /order API gateway endpoint will invoke HttpToSQS (JAXRS version) and restul /order2 will invoke HttpToSQS2 (The native lambda favor). These API will put the order to SQS.

OrderProcess are the configured to poll from SQS. It will deserialize the Order Model and put it to a dynamoDB table using DynamoDBMapper.

Sorry, this is for me to have experinemnt on AWS technology only. The code may not be cleaned up and no test is provided.

Example call (It may not work if I have to clean up my AWS account.):
###
POST https://2a6qqgvsb2.execute-api.eu-west-2.amazonaws.com/dev/order
Content-Type: application/json

{
	"orderItems": [{
		"itemId": 123,
		"itemDesc": "Gala Apple",
		"price": 50
	}],
	"totalPrice": 153
}

###
POST https://2a6qqgvsb2.execute-api.eu-west-2.amazonaws.com/dev/order2
Content-Type: application/json

{
	"orderItems": [{
		"itemId": 2,
		"itemDesc": "Banana",
		"price": 5
	}],
	"totalPrice": 5
}

TODO:
Add SNS to the game.

What I learn:
I know Java isn't a good choice for lambda because of the startup time. With these minimal project with only AWS SDK. The first call could take me more than 13s to finish. It is even worse than I imagine. Quarkus plus Graalvm native image could be the way out if I really want to use Java with Lambda. As lambda should be small, why not use nodejs or python?