package alexlib.aws.lambda.model;

/**
 * The item state at the moment the order created.
 */
public class OrderItem {
    //The original item
    private Integer itemId;
    private String itemDesc;
    private Integer price;

    public OrderItem() {
    }

    public OrderItem(Integer itemId, String itemDesc, Integer price) {
        this.itemId = itemId;
        this.itemDesc = itemDesc;
        this.price = price;
    }

    public Integer getItemId() {
        return itemId;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public Integer getPrice() {
        return price;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    

    
}
