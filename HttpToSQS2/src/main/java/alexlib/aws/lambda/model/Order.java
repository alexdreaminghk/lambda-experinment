package alexlib.aws.lambda.model;

import java.util.Date;
import java.util.List;

public class Order {
    private String orderId;
    private List<OrderItem> orderItems;
    private Integer totalPrice;
    private Date orderDate;

    public Order() {
    }

    public Order(String orderId, List<OrderItem> orderItems, Integer totalPrice, Date orderDate) {
        this.orderId = orderId;
        this.orderItems = orderItems;
        this.totalPrice = totalPrice;
        this.orderDate = orderDate;
    }



    public String getOrderId() {
        return orderId;
    }
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    public List<OrderItem> getOrderItems() {
        return orderItems;
    }
    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
    public Integer getTotalPrice() {
        return totalPrice;
    }
    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }


    public Date getOrderDate() {
        return orderDate;
    }


    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    
    
}
