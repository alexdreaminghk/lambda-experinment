package alexlib.aws.lambda;

import java.time.Clock;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import alexlib.aws.lambda.model.Order;
import alexlib.aws.lambda.model.SuccessfulResponse;

/**
 * Lambda function entry point. You can change to use other pojo type or implement
 * a different RequestHandler.
 *
 * @see <a href=https://docs.aws.amazon.com/lambda/latest/dg/java-handler.html>Lambda Java Handler</a> for more information
 */
public class App implements RequestHandler<Order, SuccessfulResponse> {
    private AmazonSQS sqsClient;
    private final Logger logger = LoggerFactory.getLogger(App.class);

    public App() {
        // Initialize the SDK client outside of the handler method so that it can be reused for subsequent invocations.
        // It is initialized when the class is loaded.
        sqsClient = DependencyFactory.SQSClient();
        // Consider invoking a simple api here to pre-warm up the application, eg: dynamodb#listTables
    }

    @Override
    public SuccessfulResponse handleRequest(final Order order, final Context context) {
        Clock clock = Clock.systemUTC();
        Date now = Date.from(clock.instant());

        //set orderTime and then
        order.setOrderDate(now);
        //save order to SQS and return 200

        String payload;
        try {
            payload = new ObjectMapper().writeValueAsString(order);
        } catch (JsonProcessingException e) {
            logger.error("Serialization failure of Order Object",e);
            throw new RuntimeException("Serialization failure");
        }

        // Map<String, MessageAttributeValue> messageAttributes = new HashMap<>();
        // messageAttributes.put("AttributeOne", new MessageAttributeValue()
        //   .withStringValue("This is an attribute")
        //   .withDataType("String")); 

        SendMessageRequest sqsRequest = new SendMessageRequest().withQueueUrl("https://sqs.eu-west-2.amazonaws.com/518750638386/orderQueue")
        .withMessageBody(payload)
        .withDelaySeconds(30);
      
        sqsClient.sendMessage(sqsRequest);

        SuccessfulResponse response = new SuccessfulResponse();
        response.setResult("Order placed successfully.");
        return response;
    }
}
