
package alexlib.aws.lambda;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
/**
 * The module containing all dependencies required by the {@link App}.
 */
public class DependencyFactory {

    private DependencyFactory() {}

    /**
     * @return an instance of sqsClient
     */
    public static AmazonSQS SQSClient() {
        return AmazonSQSClientBuilder.standard()
                    .withRegion(System.getenv("AWS_REGION"))
                    .build();
                       
    }
}
