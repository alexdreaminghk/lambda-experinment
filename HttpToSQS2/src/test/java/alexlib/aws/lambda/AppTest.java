package alexlib.aws.lambda;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import alexlib.aws.lambda.model.Order;
import alexlib.aws.lambda.model.OrderItem;

public class AppTest {

    @Test
    public void handleRequest_shouldReturnConstantValue() {
        App function = new App();
        Order order = new Order();
        order.setOrderId("123");
        List<OrderItem> orderItems = new ArrayList();
        OrderItem item1 = new OrderItem(1, "Desc", 15);
        orderItems.add(item1);
        order.setOrderItems(orderItems);
        Object result = function.handleRequest(order, null);
        assertEquals("echo", result);
    }
}
