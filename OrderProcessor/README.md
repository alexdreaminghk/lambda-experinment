# About this project

This is a lambda to handle event from SQS. The order will be deserialized from the event and will be saved to dynamoDB. This lambda will be mapped to event source mapping. The message will get poll by lambda and deleted automatically.

As this is a experinmental project, automated test is not a concern here.