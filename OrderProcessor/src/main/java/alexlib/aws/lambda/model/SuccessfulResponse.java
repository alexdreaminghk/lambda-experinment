package alexlib.aws.lambda.model;

public class SuccessfulResponse {
    private String result;

    public SuccessfulResponse() {
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    
}
