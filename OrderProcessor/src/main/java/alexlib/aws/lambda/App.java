package alexlib.aws.lambda;

import alexlib.aws.lambda.model.Order;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Lambda function entry point. You can change to use other pojo type or implement
 * a different RequestHandler.
 *
 * @see <a href=https://docs.aws.amazon.com/lambda/latest/dg/java-handler.html>Lambda Java Handler</a> for more information
 */
public class App implements RequestHandler<SQSEvent, String> {

    Logger logger = LoggerFactory.getLogger(App.class);
    private DynamoDBMapper dynamoDBMapper;
    private ObjectMapper mapper;
    //private DynamoDBMapper dynamoDBMapper;

    public App() {
        // Initialize the SDK client outside of the handler method so that it can be reused for subsequent invocations.
        // It is initialized when the class is loaded.

        dynamoDBMapper = DependencyFactory.DynamoDBMapper();
        mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        //dynamoDBMapper = new DynamoDBMapper(dynamoDBClient);
        // Consider invoking a simple api here to pre-warm up the application, eg: dynamodb#listTables
    }

    @Override
    public String handleRequest(final SQSEvent event, final Context context) {

        List<SQSEvent.SQSMessage> events = event.getRecords();
        Order order = null;

        for (SQSEvent.SQSMessage sqsEvent: events) {
            try {
                order = mapper.readValue(sqsEvent.getBody(), Order.class);
            } catch (JsonProcessingException e) {
                logger.error("Deserialization order from SQS failed", e);
            }

            dynamoDBMapper.save(order);
        }



        return "200";
    }
}
